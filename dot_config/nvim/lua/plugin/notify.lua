local notify_available, notify = pcall(require, "notify")
if not notify_available then
  return
end

notify.setup({
  background_colour = "#000000",
})
