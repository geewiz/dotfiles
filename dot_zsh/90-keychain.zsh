if [ -x "$(command -v keychain)" ]; then
  keychain --quiet --ignore-missing id_rsa
  keychain_script="$HOME/.keychain/$(hostnamectl --static)-sh"
  [ -f $keychain_script ] && source $keychain_script
fi
