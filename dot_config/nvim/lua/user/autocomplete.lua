--
-- Snippets
--

local snip_ok, luasnip = pcall(require, "luasnip")
if not snip_ok then
  return
end

luasnip.config.set_config {
  history = true,
}

require("luasnip/loaders/from_vscode").lazy_load()

-- Framework extensions
luasnip.filetype_extend("ruby", { "rails" })

--
-- Autocompletion
--

local cmp_ok, cmp = pcall(require, "cmp")
if not cmp_ok then
  return
end

-- Set completeopt to have a better completion experience
vim.opt.completeopt = { 'menu', 'menuone', 'noselect' }

local check_backspace = function()
  local col = vim.fn.col "." - 1
  return col == 0 or vim.fn.getline("."):sub(col, col):match "%s"
end

local kind_icons = {
  Text = "󰈙",
  Method = "m",
  Function = "f",
  Constructor = "",
  Field = "",
  Variable = "󰫧",
  Class = "",
  Interface = "i",
  Module = "",
  Property = "",
  Unit = "",
  Value = "a",
  Enum = "",
  Keyword = "",
  Snippet = "",
  Color = "",
  File = "",
  Reference = "",
  Folder = "",
  EnumMember = "",
  Constant = "c",
  Struct = "",
  Event = "",
  Operator = "",
  TypeParameter = "t",
  Copilot = "",
}
-- find more here: https://www.nerdfonts.com/cheat-sheet

local function contains(t, value)
  for _, v in pairs(t) do
    if v == value then
      return true
    end
  end
  return false
end

-- Allowed file types for buffer source
local disabled_fts = {
  "toml",
  "yaml",
  "json",
}

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping.scroll_docs(-1),
    ['<C-f>'] = cmp.mapping.scroll_docs(1),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping {
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    },
    ['<CR>'] = cmp.mapping.confirm { select = false },
  }),
  sources = {
    {
      name = "copilot",
      max_item_count = 3,
      trigger_characters = {
        {
          ".",
          ":",
          "(",
          "'",
          '"',
          "[",
          ",",
          "#",
          "*",
          "@",
          "|",
          "=",
          "-",
          "{",
          "/",
          "\\",
          "+",
          "?",
          " ",
        },
      },
      group_index = 2,
    },
    { name = 'nvim_lsp', group_index = 2, },
    { name = 'luasnip',  group_index = 2, },
    {
      name = 'buffer',
      group_index = 2,
    },
    { name = 'path', group_index = 2, },
  },
  confirm_opts = {
    behavior = cmp.ConfirmBehavior.Replace,
    select = false,
  },
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
      vim_item.menu = ({
        nvim_lsp = "[LSP]",
        luasnip = "[Snippet]",
        buffer = "[Buffer]",
        path = "[Path]",
      })[entry.source.name]
      return vim_item
    end,
  },
  window = {
    documentation = cmp.config.window.bordered(),
    completion = cmp.config.window.bordered(),
  },
  experimental = {
    ghost_text = true,
    native_menu = false,
  },
  enabled = function()
    -- disable completion in comments
    local context = require 'cmp.config.context'
    -- keep command mode completion enabled when cursor is in a comment
    if vim.api.nvim_get_mode().mode == 'c' then
      return true
    else
      return not context.in_treesitter_capture("comment")
          and not context.in_syntax_group("Comment")
    end
  end
}

-- Set configuration for specific filetypes
--

cmp.setup.filetype('gitcommit', {
  sources = cmp.config.sources({
    { name = 'cmp_git' },
  }, {
    { name = 'buffer' },
  })
})

cmp.setup.filetype('markdown', {
  sources = cmp.config.sources({
    { name = 'lua_snip' },
  }, {
    { name = 'path' },
  })
})

cmp.setup.filetype('text', {
  sources = cmp.config.sources({
    { name = 'lua_snip' },
  }, {
    { name = 'path' },
  })
})

-- Add ToggleAutoComplete command
local cmp_enabled = true
vim.api.nvim_create_user_command("ToggleAutoComplete", function()
  if cmp_enabled then
    require("cmp").setup.buffer({ enabled = false })
    cmp_enabled = false
  else
    require("cmp").setup.buffer({ enabled = true })
    cmp_enabled = true
  end
end, {})
