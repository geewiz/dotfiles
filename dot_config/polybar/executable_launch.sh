#!/usr/bin/env bash

# Terminate already running bar instances
polybar-msg cmd quit

if type "xrandr"; then
  IFS=$'\n'
  for m in $(xrandr --query | grep " connected"); do
    echo $m
    tray_pos=""
    monitor=$(cut -d" " -f1 <<< "$m")
    status=$(cut -d" " -f3 <<< "$m")

    if [ "$status" == "primary" ]; then
      tray_pos="right"
    fi

    MONITOR=$monitor TRAY_POSITION=$tray_pos polybar --reload bar1 &
  done
else
  polybar --reload bar1 &
fi

echo "Polybar launched."
