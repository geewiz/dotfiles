if [ -n "$ZENMODE" ]; then
  # Minimal prompt in special environments
  PS1="%F{green}%n@%m %2~ %(!.#.>)%f "
else
  # Starship prompt
  [[ -x "$(command -v starship)" ]] && eval "$(starship init zsh)"
fi
