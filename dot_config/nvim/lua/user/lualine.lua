if vim.fn.has('nvim-0.8') == 0 then
  return
end

local lualine_ok, lualine = pcall(require, "lualine")
if not lualine_ok then
  return
end

lualine.setup {
  options = {
    theme = 'dracula',
    icons_enabled = true,
  },

  sections = {
    lualine_a = {
      'mode',
    },
    lualine_b = {
      'diff',
      'diagnostics'
    },
    lualine_c = {
      {
        'filename',
        path = 1,
        file_status = true,
      },
    },
    lualine_x = { 'encoding', 'fileformat', 'filetype' },
    lualine_y = { 'progress' },
    lualine_z = { 'location' },
  },

  tabline = {
    lualine_a = {
      'buffers',
    },
    lualine_z = {
      'branch',
    },
  },

  winbar = {
  },

  extensions = {
  },
}
