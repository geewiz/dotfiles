# Reset shell history, e.g. for demos/trainings
function erase_history { local HISTSIZE=0; }

# Make directory and change into it
function mcd() {
  mkdir -p "$1" && cd "$_";
}

# cd with yazi
function y() {
  local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
  yazi "$@" --cwd-file="$tmp"
  if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
    builtin cd -- "$cwd"
  fi
  rm -f -- "$tmp"
}
