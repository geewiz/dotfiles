#!/bin/bash
#
# Usage: $0 <job_name>
#
# Create a file named `.restic-<job_name>` in your home dir
# that defines the necessary environment variables such as
#
# - RESTIC_REPOSITORY -- backup location
# - RESTIC_PASSWORD -- encryption password
# - BACKUP_OPTS -- backup options
# - PRUNE_OPTS -- pruning options
#

JOB=$1

if [ -z "$JOB" ]; then
    echo "Usage: $0 <job name>"
    exit 1
fi

BACKUP_OPTS="-q -x"
PRUNE_OPTS="--keep-hourly 25 --keep-daily 7 --keep-weekly 4 --keep-monthly 12"

# shellcheck source=/dev/null
source "${HOME}/.restic-${JOB}"

# Run preflight checks
if [[ -n "$PREFLIGHT_CHECK" ]]; then
    eval "$PREFLIGHT_CHECK"
    if [ $? -ne 0 ]; then
	echo "Preflight checks failed."
	exit 1
    fi
fi

# shellcheck disable=SC2086
echo "Backing up..."
restic backup "$HOME" $BACKUP_OPTS
echo "Cleaning up..."
restic forget --prune $PRUNE_OPTS
echo "Backup finished."
