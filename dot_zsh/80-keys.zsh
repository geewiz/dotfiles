# Search history with cursor up/down
bindkey '^[[A' up-line-or-beginning-search
bindkey '^[[B' down-line-or-beginning-search

# More homerow-friendly alternatives
bindkey "^p" up-line-or-beginning-search
bindkey "^n" down-line-or-beginning-search

# Word-wise navigation
bindkey '^[[1;5D' emacs-backward-word
bindkey '^[[1;5C' emacs-forward-word
