local plugin_available, autopairs = pcall(require, "nvim-autopairs")
if not plugin_available then
  return
end

autopairs.setup {}
