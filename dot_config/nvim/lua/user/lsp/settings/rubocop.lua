return {
  cmd = { "rubocop", "--lsp" },
  root_dir = require("lspconfig").util.root_pattern("Gemfile", ".git", "."),
}
