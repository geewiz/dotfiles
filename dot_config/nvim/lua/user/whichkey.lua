local wk_ok, wk = pcall(require, "which-key")
if not wk_ok then
  return
end

wk.add({
  { "<leader>/",   "<cmd>Telescope live_grep<CR>",                                   desc = "grep" },

  { "<leader>b",   group = "buf" },
  { "<leader>ba",  ":%bdelete<CR>",                                                  desc = "close all" },
  { "<leader>bd",  ":Bclose<CR>",                                                    desc = "close current" },
  { "<leader>bm",  ":call AppendModeline()<CR>",                                     desc = "append modeline" },
  { "<leader>bo",  ':%bdelete|edit #|normal `"<CR>',                                 desc = "close others" },
  { "<leader>bp",  ":b#<CR>",                                                        desc = "previous" },
  { "<leader>br",  "<cmd>lua MiniSessions.read()<CR>",                               desc = "previous" },
  { "<leader>bw",  "<cmd>lua MiniSessions.write('Session.vim')<CR>",                 desc = "previous" },

  { "<leader>d",   group = "diag" },
  { "<leader>da",  "<Cmd>Telescope diagnostics<CR>",                                 desc = "all" },
  { "<leader>dj",  "<Cmd>lua vim.diagnostic.goto_next()<CR>",                        desc = "next" },
  { "<leader>dk",  "<Cmd>lua vim.diagnostic.goto_prev()<CR>",                        desc = "prev" },
  { "<leader>dl",  "<Cmd>Telescope diagnostics bufnr=0<CR>",                         desc = "list" },
  { "<leader>ds",  '<Cmd>lua vim.diagnostic.open_float({ border = "rounded" })<CR>', desc = "show" },

  { "<leader>e",   "<Cmd>lua MiniFiles.open()<CR>",                                  desc = "file explorer" },

  { "<leader>f",   group = "find" },
  { "<leader>fb",  "<Cmd>Telescope buffers<CR>",                                     desc = "buffers" },
  { "<leader>fc",  "<Cmd>Telescope command_history<CR>",                             desc = "cmd history" },
  { "<leader>ff",  "<Cmd>Telescope find_files<CR>",                                  desc = "files" },
  { "<leader>fg",  "<Cmd>Telescope live_grep<CR>",                                   desc = "grep" },
  { "<leader>fh",  "<Cmd>Telescope help_tags<CR>",                                   desc = "help tags" },
  { "<leader>fo",  "<Cmd>Telescope commands<CR>",                                    desc = "commands" },
  { "<leader>fq",  "<Cmd>Telescope quickfix<CR>",                                    desc = "quickfix" },
  { "<leader>fr",  "<Cmd>Telescope oldfiles<CR>",                                    desc = "recent" },

  { "<leader>g",   group = "git" },
  { "<leader>gP",  "<cmd>Neogit push<CR>",                                           desc = "push" },
  { "<leader>gb",  "<cmd>Telescope git_branches<CR>",                                desc = "branch" },
  { "<leader>gc",  "<cmd>Neogit commit<CR>",                                         desc = "commit" },
  { "<leader>gs",  "<cmd>Neogit kind=split<CR>",                                     desc = "status" },

  { "<leader>l",   group = "lsp" },
  { "<leader>lI",  "<cmd>LspInstallInfo<CR>",                                        desc = "installer info" },
  { "<leader>la",  "<cmd>lua vim.lsp.buf.code_action()<CR>",                         desc = "action" },
  { "<leader>lf",  "<cmd>lua vim.lsp.buf.format { async = true }<CR>",               desc = "format" },
  { "<leader>lh",  "<cmd>lua vim.lsp.inlay_hint(0, nil)<CR>",                        desc = "inlay hints" },
  { "<leader>li",  "<cmd>LspInfo<CR>",                                               desc = "info" },
  { "<leader>lr",  "<cmd>lua vim.lsp.buf.rename()<CR>",                              desc = "rename" },

  { "<leader>lw",  group = "workspace" },
  { "<leader>lwa", "<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>",                desc = "add" },
  { "<leader>lwl", "<cmd>lua vim.lsp.buf.list_workspace_folder()<CR>",               desc = "list" },
  { "<leader>lwr", "<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>",             desc = "remove" },

  { "<leader>r",   group = "run" },
  { "<leader>rt",  "<cmd>belowright vsplit | term<CR>",                              desc = "terminal" },

  { "<leader>t",   group = "test" },
  { "<leader>tf",  '<cmd>TestFile<CR>',                                              desc = "file" },
  { "<leader>tn",  '<cmd>TestNearest<CR>',                                           desc = "nearest" },
  { "<leader>ts",  '<cmd>TestSuite<CR>',                                             desc = "all" },

  { "<leader>w",   group = "writing" },
  { "<leader>wh",  "<cmd>HardWrapMode<CR>",                                          desc = "hard wrap" },
  { "<leader>wo",  "<cmd>SoftWrapMode<CR>",                                          desc = "soft wrap" },
  { "<leader>wp",  "<cmd>MarkdownPreview<CR>",                                       desc = "preview" },
  { "<leader>wz",  ":ZenMode<CR>",                                                   desc = "Zen mode" },

  { "<leader>ws",  group = "spell" },
  { "<leader>ws?", "z=",                                                             desc = "suggest" },
  { "<leader>wsa", "zg",                                                             desc = "add" },
  { "<leader>wsn", "]s",                                                             desc = "next" },
  { "<leader>wsp", "[s",                                                             desc = "previous" },
  { "<leader>wss", ":setlocal spell!<CR>",                                           desc = "toggle" },
})
