# Remove /mnt/c from PATH to prevent "insecure world-writable dir" warnings
export PATH=`echo $PATH | tr ':' '\n' | awk '($0!~/mnt\/c/) {print} ' | tr '\n' ':'`
