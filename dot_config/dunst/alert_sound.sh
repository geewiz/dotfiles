#!/bin/bash

appname="$1"
summary="$2"
body="$3"
icon="$4"
urgency="$5"

case "$urgency" in
LOW)
	sound=""
	;;
NORMAL)
	sound=/usr/share/sounds/freedesktop/stereo/dialog-warning.oga
	;;
CRITICAL)
	sound=/usr/share/sounds/freedesktop/stereo/bell.oga
	;;
*)
	sound=/usr/share/sounds/freedesktop/stereo/window-attention.oga
	;;
esac

if [ -n "$sound" ]; then
	paplay $sound
fi
