local wezterm = require("wezterm");
local default_config = require("default_config")

local function getHostname()
    local hostname = ""
    local f = io.popen("/bin/hostname")
    if f then
        hostname = f:read("*a") or ""
        f:close()
        hostname = string.gsub(hostname, "\n$", "")
    end
    return hostname
end

-- default configuration
local config = default_config.config()

-- host-specific configuration
local hostname = getHostname()
local host_config_available, host_config = pcall(require, hostname .. "_config")
if host_config_available then
  config = host_config.customize(config)
end

-- support zen-mode.nvim
wezterm.on('user-var-changed', function(window, pane, name, value)
    local overrides = window:get_config_overrides() or {}
    if name == "ZEN_MODE" then
        local incremental = value:find("+")
        local number_value = tonumber(value)
        if incremental ~= nil then
            while (number_value > 0) do
                window:perform_action(wezterm.action.IncreaseFontSize, pane)
                number_value = number_value - 1
            end
            overrides.enable_tab_bar = false
        elseif number_value < 0 then
            window:perform_action(wezterm.action.ResetFontSize, pane)
            overrides.font_size = nil
            overrides.enable_tab_bar = true
        else
            overrides.font_size = number_value
            overrides.enable_tab_bar = false
        end
    end
    window:set_config_overrides(overrides)
end)

return config
