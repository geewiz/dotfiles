local wezterm = require("wezterm");
local default_config = require("default_config")

local config = default_config.config()
config.font_size = 16

return config
