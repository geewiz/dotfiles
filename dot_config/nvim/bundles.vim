if &compatible
  set nocompatible
end

call plug#begin()

" === plumbing
Plug 'nvim-lua/plenary.nvim' " required by telescope
" === bundles
Plug 'echasnovski/mini.nvim'
" === styling
Plug 'dracula/vim', {'as': 'dracula'}
Plug 'RRethy/nvim-base16'
" === screen layout
Plug 'hoob3rt/lualine.nvim'
Plug 'folke/zen-mode.nvim'
" === builds and tests
Plug 'vim-test/vim-test'
Plug 'preservim/vimux'
" === motions and navigation
Plug 'mrjones2014/smart-splits.nvim'
" === editing QOL
Plug 'windwp/nvim-autopairs'
Plug 'cappyzawa/trim.nvim'
Plug 'tpope/vim-sleuth'
Plug 'ojroques/nvim-osc52'
Plug 'gaoDean/autolist.nvim'
Plug 'romainl/vim-cool'
Plug 'stevearc/dressing.nvim'
" === folding
Plug 'kevinhwang91/promise-async'
Plug 'kevinhwang91/nvim-ufo'
" === Markdown
Plug 'preservim/vim-markdown'
Plug 'iamcco/markdown-preview.nvim', {'do': {-> mkdp#util#install()}, 'for': ['markdown', 'vim-plug']}
Plug 'andrewferrier/wrapping.nvim'
" === language parsing
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/mason.nvim', {'do': ':MasonUpdate'}
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'tamago324/nlsp-settings.nvim'
Plug 'SmiteshP/nvim-navic'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'nvim-treesitter/nvim-treesitter-context'
Plug 'RRethy/nvim-treesitter-endwise' " Ruby
" === snippets
Plug 'L3MON4D3/LuaSnip'
Plug 'rafamadriz/friendly-snippets'
" === auto-complete
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'saadparwaiz1/cmp_luasnip'
" === fuzzy finder
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-telescope/telescope.nvim', {'branch': '0.1.x'}
" === Ruby
Plug 'tpope/vim-rails'
Plug 'tpope/vim-rake'
" === key mappings
Plug 'folke/which-key.nvim'
" === Debugging nvim configs
Plug 'andrewferrier/debugprint.nvim'
" === LLM integration
Plug 'zbirenbaum/copilot.lua'
Plug 'zbirenbaum/copilot-cmp'

" external configuration
if filereadable(expand("~/.config/nvim/bundles.local.vim"))
  source ~/.config/nvim/bundles.local.vim
endif

call plug#end()
