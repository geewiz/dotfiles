-- Settings for efficient editing

-- andrewferrier/wrapping.nvim
-- Auto-configure line wrapping in text files
--

local wrapping_installed, wrapping = pcall(require, "wrapping")
if wrapping_installed then
  wrapping.setup({
    softener = { markdown = 1.3 },
  })
end

-- cappyzawa/trim.nvim
-- Auto-trim whitespace
--

local trim_installed, trim = pcall(require, "trim")
if trim_installed then
  trim.setup({
    ft_blocklist = {
      "markdown",
      "text",
    },
    patterns = {
      [[%s/\s\+$//e]],
      [[%s/\($\n\s*\)\+\%$//]],
      [[%s/\(\n\n\)\n\+/\1/]],
    },
  })
end

-- gaoDean/autolist.nvim
-- Auto-add list bullets
--

local autolist_installed, autolist = pcall(require, "autolist")
if autolist_installed then
  local autolist_ft = {
    "markdown",
    "text",
    "tex",
    "plaintex",
    "norg",
  }
  function SetupAutolist()
    autolist.setup()
    vim.keymap.set("i", "<tab>", "<cmd>AutolistTab<cr>")
    vim.keymap.set("i", "<s-tab>", "<cmd>AutolistShiftTab<cr>")
    -- vim.keymap.set("i", "<CR>", "<CR><cmd>AutolistNewBullet<cr>")
    vim.keymap.set("n", "o", "o<cmd>AutolistNewBullet<cr>")
    vim.keymap.set("n", "O", "O<cmd>AutolistNewBulletBefore<cr>")
    vim.keymap.set("i", "<tab>", "<Esc>>lla<cmd>AutolistRecalculate<cr>")
    vim.keymap.set("i", "<s-tab>", "<Esc><<cmd>AutolistRecalculate<cr>a")
    vim.keymap.set("n", "dd", "dd<cmd>AutolistRecalculate<cr>")
    vim.keymap.set("v", "d", "d<cmd>AutolistRecalculate<cr>")
  end

  local autolist_group = vim.api.nvim_create_augroup("autolist", {})
  vim.api.nvim_create_autocmd("FileType", {
    pattern = autolist_ft,
    group = autolist_group,
    callback = SetupAutolist,
  })
end

-- ojroques/vim-oscyank
-- use OSC terminal sequences for clipboard
--

local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "Y", "<Plug>OSCYank", opts)
vim.api.nvim_set_keymap("v", "Y", ":OSCYank<CR>", opts)
-- in select mode, treat as input
vim.api.nvim_del_keymap("s", "Y")

-- Make OSCYank work in tmux
vim.g.oscyank_term = 'default'

local function copy(lines, _) require('osc52').copy(table.concat(lines, '\n')) end
local function paste() return { vim.fn.split(vim.fn.getreg(''), '\n'), vim.fn.getregtype('') } end

vim.g.clipboard = {
  name = "osc52",
  copy = { ["+"] = copy, ["*"] = copy },
  paste = { ["+"] = paste, ["*"] = paste },
}

-- folke/zen-mode
--

local zen_installed, zen = pcall(require, "zen-mode")
if zen_installed then
  zen.setup({
    window = {
      backdrop = 0.95,
      width = 80,
      options = {
        cursorline = true,
        cursorcolumn = false,
      },
    },
  })
end
