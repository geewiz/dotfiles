# The geewiz dotfiles

My collection of configuration files for zsh, tmux and many other command line tools.

## Installation

I'm using [Chezmoi](https://www.chezmoi.io/) to manage my dotfiles. It's a great tool, and I highly recommend it.

To install my dotfiles, run

```bash
chezmoi init https://gitlab.com/geewiz/dotfiles.git
```

You can then see what would be changed:

```bash
chezmoi diff
```

If you're happy with the changes, you can apply them:

```bash
chezmoi apply
```

The above commands can be combined into a single command. That's how I deploy
them on a new machine:

```bash
chezmoi apply --init https://gitlab.com/geewiz/dotfiles.git
```
