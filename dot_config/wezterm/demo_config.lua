-- Custom config for demo
local M = {}

function M.customize(config)
  config.font_size = 14
  return config
end

return M
