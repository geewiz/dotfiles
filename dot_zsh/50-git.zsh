#
# Git autocompletion for custom scripts
#

_git_delete_branch () { __gitcomp_nl "$(__git_refs)" }

function acp() {
  git add .
  git commit -m "$1"
  git push
}

alias ga="git add"
alias gc="git commit"
alias gcm="git commit -m"
alias gca="git commit -a"
alias gss="git status"
alias groot='cd $(git root)'

#
# Repositories
#

function ghrepo() {
  git remote add origin $(gh repo view $1 --json sshUrl --jq .sshUrl)
}

#
# Branches
#

alias gb="git branch"

# branch selection
if which fzf >/dev/null 2>&1
then
  function gbs {
    if [[ -z "$1" ]]; then
      git branch | grep -v "^*" | fzf | head -n1 | xargs git checkout
    else
      git branch | grep -v "^*" | fzf -f "$1" | head -n1 | xargs git checkout
    fi
  }
else
  alias gbs="git branch"
fi

alias gco="git checkout"
alias gnb="git checkout -b"
alias gm="git merge"
alias grb="git rebase"
