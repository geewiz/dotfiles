#!/bin/bash

# cargo
[[ -f "$HOME/.cargo/env" ]] && source "$HOME/.cargo/env"

# atuin
if which atuin >/dev/null 2>&1; then
    eval "$(atuin init bash)"
fi
