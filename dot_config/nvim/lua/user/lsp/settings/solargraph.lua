return {
  init_options = {
    formatting = true,
  },
  settings = {
    solargraph = {
      useBundler = false,
      autoformat = true,
      completion = true,
      diagnostic = true,
      formatting = true,
      references = true,
      rename = true,
      symbols = true,
    }
  },
  flags = {
    debounce_text_changes = 150,
  }
}
