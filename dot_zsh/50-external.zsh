#
# External shell configurations
#

# Set default editor
if [[ -x "$(command -v nvim)" ]]
then
  export EDITOR=nvim
  export VISUAL=nvim
else
  export EDITOR=vim
  export VISUAL=vim
fi

# iTerm2
if [ -f "~/.iterm2_shell_integration.zsh" ]; then
    source "${HOME}/.iterm2_shell_integration.zsh"
fi

# rtfm
[[ -f ~/.rtfm.launch ]] && source ~/.rtfm.launch

# Hetzner Cloud CLI
if which hcloud >/dev/null 2>&1; then
  source <(hcloud completion zsh)
fi

# 1Password
if [[ -x "$(command -v op)" ]]; then
  [[ -f ~/.config/op/plugins.sh ]] && source ~/.config/op/plugins.sh
  eval "$(op completion zsh)"
  compdef _op op
fi

# Golang
[[ -d ${HOME}/go/bin ]] && export PATH="${PATH}:${HOME}/go/bin"

# cargo
[[ -f "${HOME}/.cargo/env" ]] && source "${HOME}/.cargo/env"

# rbenv
[[ -d "${HOME}/.rbenv/bin" ]] && export PATH="${HOME}/.rbenv/bin:${PATH}"
RBENV=$(which rbenv 2>&1)
test -x "$RBENV" && eval "$($RBENV init -)"

# Zoxide
if [ -x "$(command -v zoxide)" ]; then
  eval "$(zoxide init zsh)"
  zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'
fi

# JetBrains toolbox
[[ -d ~/.local/share/JetBrains/Toolbox ]] && export PATH="$PATH:~/.local/share/JetBrains/Toolbox/scripts"

# Nix
[[ -f $HOME/.nix-profile/etc/profile.d/nix.sh ]] && source $HOME/.nix-profile/etc/profile.d/nix.sh

# Ghostty
if [ -f "$GHOSTTY_RESOURCES_DIR/shell-integration/zsh/ghostty-integration" ]; then
    source "$GHOSTTY_RESOURCES_DIR/shell-integration/zsh/ghostty-integration"
fi
