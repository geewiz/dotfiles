#!/usr/bin/env zsh

set -e

WINDOW_MANAGER=${NESTED_WM:-i3}

case $WINDOW_MANAGER in
    i3)
	WM_OPTS="-c$HOME/.config/i3/nested"
	;;
    nscde)
	WM_OPTS=""
	;;
    *)
	echo "Unknown window manager '$WINDOW_MANAGER'"
	exit 1
esac

RESOLUTION="1920x1080"

function usage() {
    cat <<EOF
USAGE: $0 start|stop|restart|run
start Start nested $WINDOW_MANAGER in xephyr
stop Stop xephyr
restart reload $WINDOW_MANAGER in xephyr
run run command in nested $WINDOW_MANAGER
EOF
}

function wm_pid() {
    /bin/pidof $WINDOW_MANAGER | cut -d\  -f1
}

function xephyr_pid() {
    /bin/pidof Xephyr | cut -d\  -f1
}

function vnc_pid() {
    /bin/pidof x11vnc | cut -d\  -f1
}

if [ $# -lt 1 ]; then
    usage
    exit
fi

WM_BIN=`which $WINDOW_MANAGER`
XEPHYR=`which Xephyr`

test -x $WM_BIN || echo "$WINDOW_MANAGER executable not found."
test -x $XEPHYR || echo "Xephyr executable not found."

case "$1" in
    start)
	$XEPHYR -ac -br -noreset -screen $RESOLUTION :2 &
	sleep 1
	export DISPLAY=:2
	$WM_BIN $WM_OPTS &
	sleep 1
	x11vnc -display $DISPLAY -auth ~/.Xauthority &
	;;
    stop)
	echo "Stopping nested X server..."
	if [ -z $(vnc_pid) ]; then
	    echo "VNC not running: not stopped :)"
	else
	    kill $(vnc_pid)
	    echo "VNC stopped."
	fi
	if [ -z $(xephyr_pid) ]; then
	    echo "Xephyr not running: not stopped :)"
	else
	    kill $(xephyr_pid)
	    echo "Xephyr stopped."
	fi
	;;
    restart)
	echo -n "Restarting $WINDOW_MANAGER..."
	kill -s SIGHUP $(xephyr_pid)
	;;
    run)
	shift
	DISPLAY=:2.0 "$@" &
	;;
    *)
	usage
	;;
esac
