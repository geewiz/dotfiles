-- Requires nvim-treesitter installed
require('nvim-treesitter.configs').setup {
    endwise = {
        enable = true,
    },
}
