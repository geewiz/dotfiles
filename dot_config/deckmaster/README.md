# Deckmaster

## Environment variables

Set the following variables in `~/.deckmasterrc`. This file gets included in the
`deckmaster@.service` systemd unit file (see `config/systemd`).

`homeassistant-cli`:

- `HASS_SERVER`
- `HASS_TOKEN`

`obs-cli`:

- `OBS_HOST` (default "localhost")
- `OBS_PORT` (default "4444")
- `OBS_PASSWORD`
