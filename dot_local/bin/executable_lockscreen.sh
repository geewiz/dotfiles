#!/bin/bash
set -e

LOCKCMD=$1

icon="$HOME/.config/i3/lock.png"
img=$(mktemp '/tmp/XXXXXXXXXX.png')
import -window root $img
convert $img -scale 10% -scale 1000% $img
if [ -f "$icon" ]; then
    convert $img $icon -gravity southwest -composite $img
fi
$LOCKCMD -i $img
rm $img
