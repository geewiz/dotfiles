local M = {}

function M.isempty(s)
  return s == nil or s == ""
end

function M.getWords()
  return tostring(vim.fn.wordcount().words)
end

-- don't clobber register with blank lines
function M.smart_dd()
  if vim.api.nvim_get_current_line():match("^%s*$") then
    return "\"_dd"
  else
    return "dd"
  end
end

return M
