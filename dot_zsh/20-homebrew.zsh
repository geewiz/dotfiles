for brew_home in /home/linuxbrew /home/homebrew; do
  if [ -d "$brew_home/.linuxbrew" ]; then
    eval "$(${brew_home}/.linuxbrew/bin/brew shellenv)"
    break
  fi
done
