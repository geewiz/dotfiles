local f = require "user.functions"

local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

--
-- Normal --
--

-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- don't clobber register with blank lines
vim.keymap.set("n", "dd", f.smart_dd, { noremap = true, expr = true })

-- Ruby: Switch to alternate file
keymap("n", "ga", ":A<cr>", opts)

-- Fix syntax highlighting
keymap("n", "<F12>", ":syntax sync fromstart<cr>", opts)

--
-- Insert --
--

--
-- Visual --
--

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Preserve register after pasting over selection
keymap("v", "p", '"_dP', opts)

--
-- Visual Block --
--

--
-- Visual Select --
--

-- in select mode, treat as input
vim.api.nvim_del_keymap("s", "p")

--
-- Terminal --
--

-- Better terminal navigation
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

-- Simplify exiting terminal insert mode
keymap("t", "<Esc>", "<C-\\><C-n>", term_opts)
keymap("t", "<M-[>", "<C-\\><C-n>", term_opts)
