-- Hammerspoon configuration
-- based on work by Evan Travers https://github.com/evantravers/hammerspoon-config

config = {}
config.applications = {
  ['com.runningwithcrayons.Alfred'] = {
    bundleID = 'com.runningwithcrayons.Alfred',
    local_bindings = {'c', 'space', 'o', 'l'}
  },
  ['net.kovidgoyal.kitty'] = {
    bundleID = 'net.kovidgoyal.kitty',
    hyper_key = 't',
    tags = {'coding'},
    rules = {
      {nil, 1, hs.layout.maximized}
    }
  },
  ['com.brave.Browser'] = {
    bundleID = 'com.brave.Browser',
    hyper_key = 'w',
    rules = {
      {nil, 1, hs.layout.maximized},
      {"Confluence", 1, hs.layout.maximized},
      {"Meet - ", 2, hs.layout.maximized},
    }
  },
  ['com.tinyspeck.slackmacgap'] = {
    bundleID = 'com.tinyspeck.slackmacgap',
    tags = {'communication'},
    rules = {
      {nil, 2, hs.layout.maximized}
    }
  },
  ['com.apple.finder'] = {
    bundleID = 'com.apple.finder',
    hyper_key = 'f'
  },
  ['com.hnc.Discord'] = {
    bundleID = 'com.hnc.Discord',
    tags = {'distraction'},
    rules = {
      {nil, 2, hs.layout.maximized}
    }
  },
  ['com.agiletortoise.Drafts-OSX'] = {
    bundleID = 'com.agiletortoise.Drafts-OSX',
    hyper_key ='d',
    tags = {'review', 'writing', 'research', 'notes'},
    whitelisted = true,
    local_bindings = {'x', ';'}
  },
  ['com.ideasoncanvas.mindnode.macos'] = {
    bundleID = 'com.ideasoncanvas.mindnode.macos-setapp',
    tags = {'research'},
    rules = {
      {nil, 1, hs.layout.maximized}
    }
  },
  ['com.apple.MobileSMS'] = {
    bundleID = 'com.apple.MobileSMS',
    tags = {'communication', 'distraction'},
    rules = {
      {nil, 2, hs.layout.right30}
    }
  },
  ['com.valvesoftware.steam'] = {
    bundleID = 'com.valvesoftware.steam',
    tags = {'distraction'}
  },
  ['com.spotify.client'] = {
    bundleID = 'com.spotify.client'
  },
  ['md.obsidian'] = {
    bundleID = 'md.obsidian',
    hyper_key = 'n',
    tags = {'research', 'notes'},
    rules = {
      {nil, 1, hs.layout.maximized}
    }
  },
  ['us.zoom.xos'] = {
    bundleID = 'us.zoom.xos',
    rules = {
      {"Zoom Meeting", 2, hs.layout.maximized}
    }
  }
}

-- provide the ability to override config per computer
if (hs.fs.displayName('./init.local.lua')) then
  require('init.local.lua')
end

--
-- Hyper key
--

-- Enable Hyper key mods
hyper = require('hyper')
hyper.start(config)

-- Window movement mode
movewindows = require('movewindows')
movewindows.start()

-- Hammerspoon console and config reload
hyper:bind({}, 'r', nil, function()
  hs.application.launchOrFocusByBundleID('org.hammerspoon.Hammerspoon')
end)
hyper:bind({'shift'}, 'r', nil, function() hs.reload() end)

-- Lock screen with Hyper-\
hyper:bind({}, '\\', function()
  hs.caffeinate.lockScreen()
end)

-- Window focus with Hyper-HJKL
hyper:bind({}, 'H', nil, function()
  local win = hs.window.focusedWindow()
  if win then
    win:focusWindowWest()
  else
    hs.alert.show("No focused window")
  end
end)
hyper:bind({}, 'L', nil, function()
  local win = hs.window.focusedWindow()
  if win then
    win:focusWindowEast()
  else
    hs.alert.show("No focused window")
  end
end)
hyper:bind({}, 'J', nil, function()
  local win = hs.window.focusedWindow()
  if win then
    win:focusWindowSouth()
  else
    hs.alert.show("No focused window")
  end
end)
hyper:bind({}, 'K', nil, function()
  local win = hs.window.focusedWindow()
  if win then
    win:focusWindowNorth()
  else
    hs.alert.show("No focused window")
  end
end)

--
-- Mute microphone
--


function toggleMicMute()
  mic = hs.audiodevice.defaultInputDevice()
  if mic:inputMuted() then
    mic:setInputMuted(false)
    hs.alert.show("Unmuted")
  else
    mic:setInputMuted(true)
    hs.alert.show("Muted")
  end
end

hyper:bind({}, 'S', function()
  toggleMicMute()
end)

--
-- Other keybinds
--

-- Rotate external monitor
hs.loadSpoon("ToggleScreenRotation")
spoon.ToggleScreenRotation:bindHotkeys({
  first = { {"cmd"}, "f10" }
})

-- Deepl translate
hs.loadSpoon("DeepLTranslate")
spoon.DeepLTranslate:bindHotkeys({
  translate = { {"ctrl", "alt", "cmd" }, "E" }
})

-- Access Clipboard history by passing Cmd-Alt-Shift-Ctrl-V to Alfred
hyper:bind({}, 'v', function()
  hs.eventtap.keyStroke({'cmd','alt','shift','ctrl'}, 'v')
end)

-- Type clipboard contents
hs.hotkey.bind({"cmd", "alt"}, "V", function() hs.eventtap.keyStrokes(hs.pasteboard.getContents()) end)

-- Done!
hs.alert.show("Config loaded")
