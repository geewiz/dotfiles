local w = require("wezterm");
local act = w.action

local M = {}

local function is_vim(pane)
  -- this is set by the plugin, and unset on ExitPre in Neovim
  return pane:get_user_vars().IS_NVIM == 'true'
end

local direction_keys = {
  h = 'Left',
  j = 'Down',
  k = 'Up',
  l = 'Right',
}

local function split_nav(resize_or_move, key)
  return {
    key = key,
    mods = resize_or_move == 'resize' and 'META' or 'CTRL',
    action = w.action_callback(function(win, pane)
      if is_vim(pane) then
        -- pass the keys through to vim/nvim
        win:perform_action({
          SendKey = { key = key, mods = resize_or_move == 'resize' and 'META' or 'CTRL' },
        }, pane)
      else
        if resize_or_move == 'resize' then
          win:perform_action({ AdjustPaneSize = { direction_keys[key], 3 } }, pane)
        else
          win:perform_action({ ActivatePaneDirection = direction_keys[key] }, pane)
        end
      end
    end),
  }
end

function M.config()
  local config = {}

  -- Appearance
  config.color_scheme = "Dracula"
  config.window_background_opacity = 1.0
  config.hide_tab_bar_if_only_one_tab = true
  config.default_cursor_style = "BlinkingUnderline"

  -- Fonts
  config.font = w.font_with_fallback({
    { family = "Operator Mono SSm", weight = "Book" },
    { family = "Fira Code" },
    { family = "Material Icons",    weight = "Regular" },
  })
  config.font_size = 12
  config.adjust_window_size_when_changing_font_size = false

  -- Hyperlinks
  config.hyperlink_rules = w.default_hyperlink_rules()

  -- Display current workspace
  w.on('update-right-status', function(window, pane)
    window:set_right_status(window:active_workspace())
  end)

  -- Keybinds
  config.leader = { key = 'a', mods = 'CTRL', timeout_milliseconds = 1000 }
  config.keys = {
    -- Send Ctrl+a (which is assigned as leader key)
    {
      key = 'a',
      mods = 'LEADER|CTRL',
      action = act.SendKey { key = 'a', mods = 'CTRL' },
    },
    -- Send Ctrl+l (which is assigned to split navigation)
    {
      key = 'l',
      mods = 'LEADER|CTRL',
      action = act.SendKey { key = 'l', mods = 'CTRL' },
    },
    -- Splits
    {
      key = '-',
      mods = 'LEADER',
      action = act.SplitVertical { domain = 'CurrentPaneDomain' },
    },
    {
      key = '|',
      mods = 'LEADER|SHIFT',
      action = act.SplitHorizontal { domain = 'CurrentPaneDomain' },
    },
    split_nav('move', 'h'),
    split_nav('move', 'j'),
    split_nav('move', 'k'),
    split_nav('move', 'l'),
    split_nav('resize', 'h'),
    split_nav('resize', 'j'),
    split_nav('resize', 'k'),
    split_nav('resize', 'l'),
    -- Tabs
    {
      key = 'c',
      mods = 'LEADER',
      action = act.SpawnTab('CurrentPaneDomain'),
    },
    -- Workspaces
    {
      key = 'w',
      mods = 'LEADER',
      action = act.PromptInputLine {
        description = w.format {
          { Attribute = { Intensity = 'Bold' } },
          { Foreground = { AnsiColor = 'Fuchsia' } },
          { Text = 'Enter name for new workspace' },
        },
        action = w.action_callback(
          function(window, pane, line)
            -- line will be `nil` if they hit escape without entering anything
            -- An empty string if they just hit enter
            -- Or the actual line of text they wrote
            if line then
              window:perform_action(
                act.SwitchToWorkspace {
                  name = line,
                },
                pane
              )
            end
          end
        ),
      },
    },
    -- Copy mode
    {
      key = "[",
      mods = "LEADER",
      action = act.ActivateCopyMode
    },
  }

  return config
end

return M
