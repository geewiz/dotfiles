local copilot_available, copilot = pcall(require, "copilot")
if not copilot_available then
  return
end

copilot.setup({
  suggestion = { enabled = false },
  panel = { enabled = false },
  filetypes = {
    markdown = false,
  },
})

local copilot_cmp_available, copilot_cmp = pcall(require, "copilot_cmp")
if not copilot_cmp_available then
  return
end

copilot_cmp.setup()
