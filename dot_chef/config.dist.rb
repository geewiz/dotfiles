log_level :info
log_location STDOUT
node_name "geewiz"
client_key "/home/geewiz/.chef/geewiz.pem"
validation_key "/home/geewiz/.chef/NONEXISTING.pem"
chef_server_url "https://chef.example.com"
cache_type "BasicFile"
cache_options(path: "/home/geewiz/.chef/checksums")
cookbook_path [
  "/home/geewiz/Projects/freistil/chef-cookbooks/src",
  "/home/geewiz/Projects/freistil/chef-cookbooks/vendor",
]

if defined?(ChefDK)
  chefdk.generator.license = "apache2"
  chefdk.generator.copyright_holder = "freistil IT Ltd"
  chefdk.generator.email = "cookbooks@freistil.it"
end
