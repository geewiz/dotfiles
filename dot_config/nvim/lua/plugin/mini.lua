-- mini.ai - Extend and create a/i textobjects
-- https://github.com/echasnovski/mini.ai

require('mini.ai').setup(test)

-- mini.align
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-align.md

require('mini.align').setup()

-- mini.comment
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-comment.md

require('mini.comment').setup()

-- mini.diff
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-diff.md

require('mini.diff').setup()

-- mini.files
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-files.md

require('mini.files').setup()

-- mini.fuzzy
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-fuzzy.md

require('mini.fuzzy').setup()

-- mini.git
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-git.md

require('mini.git').setup()

-- mini.icons
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-icons.md

require('mini.icons').setup()

-- mini.jump
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-jump.md

require('mini.jump').setup()

-- mini.move
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-move.md

require('mini.move').setup()

-- mini.notify
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-notify.md

require('mini.notify').setup()

-- mini.sessions
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-sessions.md

require('mini.sessions').setup()

-- mini.starter
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-starter.md

require('mini.starter').setup()

-- mini.surround
-- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-surround.md

require('mini.surround').setup()
