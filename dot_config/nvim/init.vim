"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Workaround for Inspect error
" https://github.com/neovim/neovim/issues/31675
if has('nvim-0.10.3')
	lua vim.hl = vim.highlight
endif

" Let's leave the dark ages behind
set nocompatible

" Load plugins
if filereadable(expand("~/.config/nvim/bundles.vim"))
	source ~/.config/nvim/bundles.vim
endif

" Sets how many lines of history VIM has to remember
set history=700

" Enable filetype plugins
filetype plugin on
filetype indent on

" Set to auto-read when a file is changed from the outside
set autoread

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set 7 lines to the cursor - when moving vertically using j/k
set scrolloff=7

" Turn on the WiLd menu
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
	set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
else
	set wildignore+=.git\*,.hg\*,.svn\*
endif

" A buffer becomes hidden when it is abandoned
set hidden

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
	set mouse=a
endif

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set matchtime=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=

" Line numbers
set number
set relativenumber

" Display extra whitespace
set list listchars=tab:»·,trail:·,nbsp:·

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Tab line, status line and winbar
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" No need to show mode, we use a statusline.
set noshowmode

" Display only a single status line for all windows
if has('nvim-0.7')
	set laststatus=3
	highlight WinSeparator guibg=None
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors
set background=dark
let g:dracula_colorterm = 0

try
	colorscheme dracula
catch
endtry

" Use italic font
highlight Comment gui=italic
highlight Comment cterm=italic
highlight htmlArg gui=italic
highlight htmlArg cterm=italic

" Enable syntax highlighting
syntax enable

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowritebackup
set noswapfile

set undodir=~/.vim/undodir
set undofile

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set fileformats=unix,dos,mac

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Tabs, spaces, indent, line length
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Prefer tabs for better a11y
set autoindent
set nosmartindent
set noexpandtab
set copyindent
set preserveindent
set tabstop=4
set shiftwidth=4
set softtabstop=0

set textwidth=80

" Turn 2 spaces into 1 tab: `:%SuperRetab 2`
command! -nargs=1 -range SuperRetab <line1>,<line2>s/\v%(^ *)@<= {<args>}/\t/g

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Treat long lines as break lines (useful when moving around in them)
nnoremap <expr> j v:count ? 'j' : 'gj'
nnoremap <expr> k v:count ? 'k' : 'gk'

" Return to last edit position when opening files
autocmd BufReadPost *
	\ if line("'\"") > 0 && line("'\"") <= line("$") |
	\   exe "normal! g`\"" |
	\ endif

" Center cursor while paging
nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz

" Cursor behaviour at start/end of lines
set whichwrap+=<,>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Make Y behave like the rest of them
nnoremap Y y$

" Keep cursor position on join
nnoremap J mzJ`z

" Configure backspace so it acts as it should act
set backspace=eol,start,indent

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Searching
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Keep the cursor centered while searching
nnoremap n nzzzv
nnoremap N Nzzzv

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Markdown
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufNewFile,BufRead *.md set filetype=markdown
autocmd FileType markdown setlocal cursorline scrolloff=12
autocmd FileType markdown setlocal spell
autocmd FileType markdown setlocal expandtab tabstop=4 shiftwidth=4
autocmd FileType markdown setlocal list listchars=eol:⏎,tab:»·,trail:·,nbsp:·
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_autowrite = 1
let g:vim_markdown_edit_url_in = 'tab'
let g:vim_markdown_follow_anchor = 1
" header folding
let g:vim_markdown_folding_disabled = 0
let g:vim_markdown_folding_level = 1
" Syntax concealing
autocmd FileType markdown setlocal conceallevel=2
let g:vim_markdown_conceal = 2
let g:vim_markdown_conceal_code_blocks = 0
" support front matter of various formats
let g:vim_markdown_frontmatter = 1  " for YAML format
let g:vim_markdown_toml_frontmatter = 1  " for TOML format
let g:vim_markdown_json_frontmatter = 1  " for JSON format
" disable math tex conceal feature
let g:tex_conceal = ""
let g:vim_markdown_math = 1
" Auto-bullets and indent
let g:vim_markdown_auto_insert_bullets = 1
let g:vim_markdown_new_list_item_indent = 1
" Disable indentline in Markdown files to avoid concealing the formatting
let g:indentLine_fileTypeExclude = ['markdown']

" markdown-preview.nvim
" Don't close preview when switching buffers
let g:mkdp_auto_close = 0
" Open browser via xdg-open
let g:mkdp_browser = 'xdg-open'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Git
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Always start on line 1 of Git commit messages
autocmd FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])
" Limit commit messages to 72 character lines
autocmd BufRead,BufNewFile COMMIT_EDITMSG setlocal textwidth=72

" shortcuts for 3-way merge
map <leader>m1 :diffget LOCAL<CR>
map <leader>m2 :diffget BASE<CR>
map <leader>m3 :diffget REMOTE<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => vim-json
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vim_json_syntax_conceal = 0

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Include LUA configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
:lua require "user.keymaps"
:lua require "user.whichkey"
:lua require "user.lualine"
:lua require "user.nvim-ufo"
:lua require "user.telescope"
:lua require "user.editing"
:lua require "user.testing"
:lua require "user.treesitter"
:lua require "user.copilot"
:lua require "user.obsidian"
:lua require "user.autocomplete"
:lua require "user.lsp"
:lua require "user.gen"
:lua require "plugin.nvim-autopairs"
:lua require "plugin.nvim-treesitter-endwise"
:lua require "plugin.notify"
:lua require "plugin.mini"
:lua require "plugin.smart-splits"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => In-editor terminal
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
:lua require "user.terminal"

" Start in insert mode
autocmd BufEnter * if &buftype == 'terminal' | :startinsert | endif
autocmd TermOpen * startinsert

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Don't close window when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
	let l:currentBufNum = bufnr("%")
	let l:alternateBufNum = bufnr("#")

	if buflisted(l:alternateBufNum)
		buffer #
	else
		bnext
	endif

	if bufnr("%") == l:currentBufNum
		new
	endif

	if buflisted(l:currentBufNum)
		execute("bdelete! ".l:currentBufNum)
	endif
endfunction

" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
	let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d %set :",
		\ &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
	let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
	call append(line("$"), l:modeline)
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Include local configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if filereadable($HOME . "/.config/nvim/init.local.vim")
	source ~/.config/nvim/init.local.vim
endif
