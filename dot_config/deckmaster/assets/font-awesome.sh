#!/bin/bash
# Convert Font Awesome SVG into PNG
#
# Usage: ./font-awesome.sh /path/to/font-awesome-desktop

if [ $# -ne 1 ]; then
	echo "Usage: $0 /path/to/font-awesome-desktop"
	exit 1
fi

for icon in \
	user-secret \
	clapperboard-play \
	folder-open \
	heat \
	house \
	lightbulb \
	lightbulb-on \
	volume-off \
	volume-low \
	volume-high \
	play \
	play-pause \
	brightness \
	brightness-low \
	face-sleeping \
	screwdriver-wrench \
	video \
	signal-stream \
	; do
	convert MSVG:$1/svgs/solid/$icon.svg \
		-fill orange -opaque black -fill transparent -opaque white \
		-thumbnail 128x128 -gravity center -background transparent -extent 128x128 \
		$icon.png
done
